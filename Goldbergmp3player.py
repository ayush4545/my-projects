from tkinter import *
from tkinter import filedialog
import os
from pygame import mixer
import time
from tkinter.ttk import Progressbar
from mutagen.mp3 import MP3

mixer.init()
play_status = False
song_length = 0
song_no = 0


def play_song():
    global song_no
    selected_song = playlist.curselection()
    selected_song = int(selected_song[0])
    song_no = selected_song
    play_it = play_list[song_no]
    mixer.music.load(play_it)
    mixer.music.play()
    file_data = os.path.splitext(play_it)
    if file_data[1] == ".mp3":
        audio = MP3(play_it)
        song_length = audio.info.length
    else:
        a = mixer.Sound(play_it)
        song_length = a.get_length()

    minute, second = divmod(song_length, 60)
    minute = round(minute)
    second = round(second)
    timeformate = "{:02d}:{:02d}".format(minute, second)
    total_time["text"] = timeformate

    progress_status()
    text = name[song_no]
    text = text.split('-')
    print(text)
    songtrack['text'] = text[1]
    status_label['text'] = 'Music Playing...'
    playlist.activate(song_no)
    playlist.itemconfigure(song_no, bg='lightblue')


def playpause():
    global play, pause, play_status
    if play_status:
        play_pause['image'] = pause
        mixer.music.unpause()
        play_status = FALSE
        status_label['text'] = 'Music Pause'
        progress_status()
        print('pause')
    else:
        play_pause['image'] = play
        mixer.music.pause()
        play_status = TRUE
        status_label['text'] = 'Music Playing...'
        progress_status()
        print('play')


filenamepath = ''
play_list = []
name = []


def load():
    global filenamepath
    try:
        filenamepath = filedialog.askopenfilename(initialdir='D:\Songs\sounds',
                                                  title='Select Audio file',
                                                  filetype=(('MP3', '*.mp3'), ('WAV', '*.wav')))
        print(filenamepath)
        add_to_playlist(filenamepath)
    except:
        filenamepath = filedialog.askopenfilename(title='Select Audio file',
                                                  filetype=(('MP3', '*.mp3'), ('WAV', '*.wav')))
        print(filenamepath)
        add_to_playlist(filenamepath)


def add_to_playlist(filename):
    filename = os.path.basename(filename)
    index = 0
    playlist.insert(index, filename)
    play_list.insert(index, filenamepath)
    name.insert(index, filename)
    print('playlist: ', filenamepath)
    index += 1


def delete_song():
    selected_song = playlist.curselection()
    selected_song = int(selected_song[0])
    playlist.delete(selected_song)
    play_list.pop(selected_song)


def stop_music():
    mixer.music.stop()
    status_label['text'] = 'Music Stop'
    timebar['value']=0
    start_time['text']='00:00'


def previous_song():
    global song_no
    if song_no > 0:
        song_no -= 1
    else:
        song_no = 0
    mixer.music.stop()
    time.sleep(1)
    play_it = play_list[song_no]
    mixer.music.load(play_it)
    mixer.music.play()
    file_data = os.path.splitext(play_it)
    if file_data[1] == ".mp3":
        audio = MP3(play_it)
        song_length = audio.info.length
    else:
        a = mixer.Sound(play_it)
        song_length = a.get_length()

    minute, second = divmod(song_length, 60)
    minute = round(minute)
    second = round(second)
    timeformate = "{:02d}:{:02d}".format(minute, second)
    total_time["text"] = timeformate

    progress_status()
    text = name[song_no]
    text = text.split('-')
    print(text)
    songtrack['text'] = text[1]
    status_label['text'] = 'Music Playing...'
    playlist.activate(song_no)
    playlist.itemconfigure(song_no, bg='lightblue')
    playlist.itemconfigure(song_no + 1, bg='white')


def next_song():
    global song_no
    if song_no < len(play_list):
        song_no += 1
    else:
        song_no = len(play_list) - 2
    mixer.music.stop()
    time.sleep(1)
    play_it = play_list[song_no]
    mixer.music.load(play_it)
    mixer.music.play()
    file_data = os.path.splitext(play_it)
    if file_data[1] == ".mp3":
        audio = MP3(play_it)
        song_length = audio.info.length
    else:
        a = mixer.Sound(play_it)
        song_length = a.get_length()

    minute, second = divmod(song_length, 60)
    minute = round(minute)
    second = round(second)
    timeformate = "{:02d}:{:02d}".format(minute, second)
    total_time["text"] = timeformate

    progress_status()
    text = name[song_no]
    text = text.split('-')
    print(text)
    songtrack['text'] = text[1]
    status_label['text'] = 'Music Playing...'
    playlist.activate(song_no)
    playlist.itemconfigure(song_no, bg='lightblue')
    playlist.itemconfigure(song_no - 1, bg='white')


mute = True


def mute():
    global mute, mutepic, speaker
    if mute:
        mute_unmute['image'] = mutepic
        mixer.music.set_volume(0)
        volumeprogress['value'] = 0
        scale.set(0)
        mute = False
    else:
        mute_unmute['image'] = speaker
        mixer.music.set_volume(0.7)
        volumeprogress['value'] = 70
        scale.set(70)
        mute = True


def volume(value):
    volume = float(value) / 100
    mixer.music.set_volume(volume)
    volumeprogress['value'] = value


def progress_status():
    current_time = mixer.music.get_pos() // 1000
    timebar['value'] = current_time
    minute, second = divmod(current_time, 60)
    minute = round(minute)
    second = round(second)
    timeformate = "{:02d}:{:02d}".format(minute, second)
    start_time["text"] = timeformate

    timebar.after(1000, progress_status)


root = Tk()
root.geometry('650x700')
root.resizable(width=False, height=False)
root.title('Goldberg MP3 Player')
root.wm_iconbitmap('icon.ico')
root.configure(bg='grey')
ss = "Developed By Ayush Mishra"
l1 = Label(root, font=("verdana", 15, "bold"), text='',bg='grey',fg='white')
l1.grid(row=0, column=0, padx=30, pady=5, columnspan=2)
s = ''
count = 0


def slider():
    global s, count
    if count >= len(ss):
        count = -1
        s = ''
        l1['text'] = s
    else:
        s = s + ss[count]
        l1['text'] = s
    count += 1

    l1.after(200, slider)


slider()

track = LabelFrame(root, text='Song Track',
                   font=("times new roman", 15, "bold"),
                   bg="grey", fg="white", bd=5, relief=GROOVE)
track.config(width=410, height=300)
track.grid(row=1, column=0, padx=15)
pic = PhotoImage(file='music.gif')
img = Label(track, image=pic)
img.config(width=400, height=240)
img.grid(row=0, column=0)

songtrack = Label(track, font=("times new roman", 16, "bold"),
                  bg="white", fg="dark blue")
songtrack['text'] = 'Goldberg MP3 Player'
songtrack.config(width=30, height=1)
songtrack.grid(row=1, column=0, padx=10)

tracklist = LabelFrame(root, text=f'PlayList',
                       font=("times new roman", 15, "bold"),
                       bg="grey", fg="white", bd=5, relief=GROOVE)

tracklist.config(width=190, height=300)
tracklist.grid(row=1, column=1, rowspan=3, pady=5)

scrollbar = Scrollbar(tracklist, orient=VERTICAL)
scrollbar.grid(row=0, column=1, rowspan=5, sticky='ns')
playlist = Listbox(tracklist, selectmode=SINGLE,
                   yscrollcommand=scrollbar.set, selectbackground='sky blue')
playlist.config(height=17, width=26)
scrollbar.config(command=playlist.yview)
playlist.grid(row=0, column=0, rowspan=5)

status_label = Label(root, text='', font=("times new roman", 20, "bold"),bg='grey',fg='white')
status_label.place(x=155, y=360)

button_frame = Frame(root, width=200, height=75, bg="grey")
button_frame.place(x=120, y=420)
play = PhotoImage(file='play.png')
pause = PhotoImage(file='pause.png')
play_pause = Button(button_frame, image=pause, width=60, height=60, command=playpause,bg='grey')
play_pause.grid(row=1, column=1, padx=5, pady=5)

nxt = PhotoImage(file='next.png')
nxtbtn = Button(button_frame, image=nxt, width=60, height=60, command=next_song,bg='grey')
nxtbtn.grid(row=1, column=2, padx=5, pady=5)

pre = PhotoImage(file='pre.png')
prebtn = Button(button_frame, image=pre, width=60, height=60, command=previous_song,bg='grey')
prebtn.grid(row=1, column=0, padx=5, pady=5)

start = PhotoImage(file='start.png')
startbtn = Button(button_frame, image=start, width=60, height=60, command=play_song,bg='grey')
startbtn.grid(row=0, column=0, padx=5, pady=5, columnspan=2)

stop = PhotoImage(file='end.png')
stopbtn = Button(button_frame, image=stop, width=60, height=60, command=stop_music,bg='grey')
stopbtn.grid(row=0, column=1, padx=5, pady=5, columnspan=2)

add = Button(root, text='Load Song', command=load,bg='grey',fg='white')
add.place(x=450, y=370)

delete = Button(root, text='Delete', width=8, command=delete_song,bg='grey',fg='white')
delete.place(x=550, y=370)

speaker = PhotoImage(file='speaker.png')
mutepic = PhotoImage(file='mute.png')
mute_unmute = Button(root, image=speaker, command=mute,bg='grey')
mute_unmute.place(x=565, y=620)

vol_progress_label = Label(root, bg='red')
vol_progress_label.place(x=480, y=440)

scale = Scale(vol_progress_label, from_=100, to=0, orient=VERTICAL, sliderlength=5, cursor='circle', troughcolor='grey',
              command=volume)
scale.set(70)
mixer.music.set_volume(0.7)
scale.grid(row=0, column=1, padx=1)

volumeprogress = Progressbar(vol_progress_label, orient=VERTICAL,
                             length=106, mode='determinate')
volumeprogress['value'] = 70
volumeprogress.grid(row=0, column=0, padx=1)

time_progress_label = Label(root, bg='lightgrey')
time_progress_label.place(x=15, y=623)

start_time = Label(time_progress_label, text='00:00',bg='lightgrey')
start_time.grid(row=0, column=0, pady=5, padx=5)

timebar = Progressbar(time_progress_label, orient=HORIZONTAL, mode='determinate')
timebar.grid(row=0, column=1, pady=5, padx=5, ipadx=170)

total_time = Label(time_progress_label, text='00:00',bg='lightgrey')
total_time.grid(row=0, column=2, pady=5, padx=5)

root.mainloop()
