import sys
from cx_Freeze import *

includefiles = ['icon.ico', 'music.gif', 'end.png', 'mute.png', 'pause.png', 'play.png', 'pre.png', 'speaker.png',
                'start.png', 'next.png']
excludes = []
packages = []
base = None
if sys.platform == 'win32':
    base = 'win32GUI'

shortcut_table = [
    ('DesktopShortcut',
     'DesktopFolder',
     'Golderberg Mp3 Player',
     'TARGETDIR',
     '[TARGETDIR]\Goldbergmp3player.exe',
     None,
     None,
     None,
     None,
     None,
     None,
     'TARGETDIR'

     )
]
msi_data={'Shortcut':shortcut_table}
bdist_msi_options={'data':msi_data}

setup(
    version='0.1',
    description='Developed By Ayush Mishra',
    author='Ayush Mishra',
    name='Golderberg',
    options={'build_exe':{'includes_files':includefiles},'bdist_msi':bdist_msi_options,},
    executables=[
        Executable(
            script='Goldbergmp3player.py',
            base=base,
            icon='icon.ico',
        )
    ]

)
